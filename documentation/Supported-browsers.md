The annotations tool supports the following browsers (given version or more recent):
* Firefox 3.6 
* Internet Explorer 9
* Safari 5 
* Chrome 14

Mobile browsers on iOS or Android platforms are not supported.